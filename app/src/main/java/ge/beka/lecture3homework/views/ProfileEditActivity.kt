package ge.beka.lecture3homework.views

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.widget.EditText
import ge.beka.lecture3homework.R
import ge.beka.lecture3homework.models.UserModel
import kotlinx.android.synthetic.main.activity_profile_edit.*

class ProfileEditActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_edit)
        init()
    }

    private fun init() {
        fillUserInfo()
        saveChangesButton.setOnClickListener {
            saveChanges()
        }
    }

    private fun fillUserInfo(){
        val userModel = intent.getParcelableExtra("userData") as UserModel
        firstNameEditText.setText(userModel.firstName)
        lastNameEditText.setText(userModel.lastName)
        emailEditText.setText(userModel.email)
        sexEditText.setText(userModel.sex)
        d("fillUserInfo", String.format("Filling activity with user info: %s", userModel.toString()))
    }

    private fun saveChanges() {
        val userModel = UserModel(
            getEditableString(firstNameEditText), getEditableString(lastNameEditText),
            getEditableString(emailEditText), getEditableString(sexEditText)
        )

        val intent = intent
        intent.putExtra("userData", userModel)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    private fun getEditableString(editable: EditText): String {
        return editable.text.toString()
    }


}
