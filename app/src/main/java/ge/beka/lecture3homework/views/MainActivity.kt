package ge.beka.lecture3homework.views

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log.d
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import ge.beka.lecture3homework.R
import ge.beka.lecture3homework.models.UserModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val EditProfileCode = 1999

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }


    private fun init(){
        changeProfileParameters.setOnClickListener{
            openProfileEditor()
        }
    }

    private fun openProfileEditor(){
        val intent = Intent(this, ProfileEditActivity::class.java)

        val userModel = UserModel(
            getTextViewString(firstNameTextView), getTextViewString(lastNameTextView),
            getTextViewString(emailTextView), getTextViewString(sexTextView)
        )

        intent.putExtra("userData", userModel)
        startActivityForResult(intent, EditProfileCode)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == EditProfileCode && resultCode == Activity.RESULT_OK){
            val userData = data!!.getParcelableExtra("userData") as UserModel
            d("updateUserInfo", userData.toString())
            firstNameTextView.text = userData.firstName
            lastNameTextView.text = userData.lastName
            emailTextView.text = userData.email
            sexTextView.text = userData.sex
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun getTextViewString(textView: TextView) : String{
        return textView.text.toString()
    }
}
