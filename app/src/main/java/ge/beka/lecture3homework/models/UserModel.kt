package ge.beka.lecture3homework.models

import android.os.Parcel
import android.os.Parcelable

class UserModel(val firstName: String, val lastName: String, val email: String, val sex: String) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readString().toString(),
        parcel.readString().toString(),
        parcel.readString().toString(),
        parcel.readString().toString()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(firstName)
        parcel.writeString(lastName)
        parcel.writeString(email)
        parcel.writeString(sex)
    }

    override fun toString(): String {
        return String.format("\nFirst name: %s\nLast name: %s\nEmal: %s\nSex: %s", firstName, lastName, email, sex)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<UserModel> {
        override fun createFromParcel(parcel: Parcel): UserModel {
            return UserModel(parcel)
        }

        override fun newArray(size: Int): Array<UserModel?> {
            return arrayOfNulls(size)
        }
    }
}